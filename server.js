

function init(route, handler){ //Pasamos el objeto route y el objeto handler
	var server = require('http').createServer();
	var url = require('url');
	
	function control(req,resp){
		var pathName = url.parse(req.url).pathname;
		console.log('Petición recibida');
		route(handler, pathName, resp);
		resp.writeHead(200, {'content-type':'text/plain'});
		resp.write('Peticion recibida en el servidor: '+pathName);
		resp.end();
	}
	
	server.on('request',control).listen(8050);
	console.log('Servidor inicializado');
}

//Exportamos la función
exports.init = init; 



/*
//Ejemplo inicial de GeekyTheory
var http = require('http');
var server = http.createServer();

function control(req,resp){
	console.log('Petición recibida');
	resp.writeHead(200, {'content-type':'text/plain'});
	resp.write('Petición recibida en el servidor.');
	resp.end();
}

server.on('request',control);
server.listen(8050);
console.log('Servidor inicializado');
*/

/* 
//Ejemplo inicial de nodejs.org
var http = require('http');
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World\n');
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
*/

