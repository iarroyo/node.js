

var server = require('./server'); //Importamos el módulo server
var router = require('./router'); //Importamos el módulo router
var handlers = require('./handler'); //Importamos el módulo handler
var handler = {};

handler["/"] = handlers.funcion_1;
handler["/funcion_1"] = handlers.funcion_1;
handler["/funcion_2"] = handlers.funcion_2;

//Llama al método init del objeto server
server.init(router.route, handler);


